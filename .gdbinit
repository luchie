define hook-quit
  set confirm off
end

set history save on
set confirm off
set print asm-demangle on

set remote hardware-breakpoint-limit 4
set remote hardware-watchpoint-limit 2
target extended-remote psyduck:3334

monitor arm semihosting enable
mon reset init
