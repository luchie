#![no_std]

pub mod cirque;
pub mod event_filter;
pub mod logger;

#[cfg(test)]
mod tests {
    #[test]
    fn pass() {
        assert!(0 == 0);
    }

    #[test]
    #[should_panic]
    fn fail() {
        assert!(1 == 0);
    }
}
